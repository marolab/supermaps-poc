import React from 'react';
import MapComponent from './components/MapComponent'
import './App.css';

export default (props) =>
  <div>
    <MapComponent />
  </div>
