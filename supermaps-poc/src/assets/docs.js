const About = "https://gitlab.com/marolab/supermaps-poc/blob/master/supermaps-poc/public/about-super-map.pdf"
const Help = "https://gitlab.com/marolab/supermaps-poc/blob/master/supermaps-poc/public/help-super-map.pdf"

export { About, Help }