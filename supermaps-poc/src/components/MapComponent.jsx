import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import './MapComponent.css'
import { About, Help } from '../assets/docs'

export class MapContainer extends Component {

  constructor(props) {
    super(props);
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeLat = this.onChangeLat.bind(this);
    this.onChangeLng = this.onChangeLng.bind(this);

    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      tex: '',
      lat: 0.0,
      lng: 0.0
    };
  }

  onMarkerClick(props, marker, e) {
    this.setState( {
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    }
    );
}

  onChangeText(e) {
    this.setState( {tex: e.target.value} )
  }

  onChangeLat(e) {
    this.setState( {lat: e.target.value} )
  }

  onChangeLng(e) {
    this.setState( {lng: e.target.value} )
  }


  render() {
    const coords = { lat: -29.689193, lng: -51.469312 };

    if (!this.props.google) {
      return <div>Carregando Supermap...</div>;
    }

    return (

        <div id="general">
          <form>
            <div>
              <span>
                <h2 id="titulo">Super Map</h2>
                <a style={{ marginLeft:"90%", position: "relative"}}
                   href={About}
                   target="_blank">About</a>
                <a style={{ marginLeft:"2%", position: "relative"}}
                   href={Help}
                   target="_blank">Help</a>
              </span>
            </div>
          <div>
              <label class="inx">Local :
              <input type="text"
                       className="tex"
                       style={{ width: "400px"}}
                       onChange={(e) => this.onChangeText(e)}/>
              </label>
              <label class="inx">Latitude :
                  <input type="number"
                         className="lat"
                         step="any"
                         onChange={(e) => this.onChangeLat(e)}/>
              </label>
              <label class="inx">Longitude :
                  <input type="number"
                         className="lng"
                         step="any"
                         onChange={(e) => this.onChangeLng(e)}/>
              </label>
          <br/>
          </div>
        </form><br/>

        <Map google={this.props.google}
             zoom={14}
             initialCenter={coords}
        >
          <Marker
            title={{ tex: this.state.tex }}
            position={{ lat: this.state.lat, lng: this.state.lng }}
            onClick={ this.onMarkerClick }
          />
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
            >
              <div>
                <h1>{this.state.tex}</h1>
              </div>
          </InfoWindow>
        </Map>
       </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDTcp9OSiIf3tnOMocbIAwt3_E2ftIHNkE",
  v: "3.30"
})(MapContainer);


/** a apiKey (AIzaSyDTcp9OSiIf3tnOMocbIAwt3_E2ftIHNkE) - pertence a Maroli Becker Junior - 20/12/2019)
 * pela conta mbeckerj@gmail.com
*/